from flask import render_template
from app import app

author = {
	'title': 'Herr',
	'firstname':'Mario',
	'lastname':'Tormo Romero',
	'email': 'mario@someemail.com',
	'url' : 'www.google.com'
}
@app.route('/')
@app.route('/index')
def index():
	#user = {'nickname': 'Michael'}  # Not a real user
	content = "Some static welcome message"
	return render_template('index.html',
		title='Home', 
		content=content)
@app.route('/about')
def about():
	return render_template('about.html', title="About",author=author)
@app.route('/chart')
def chart():
    legend = 'Monthly Data'
    labels = ["January", "February", "March", "April", "May", "June", "July", "August"]
    values = [10, 9, 8, 7, 6, 4, 7, 8]
    return render_template('charts.html', title="Charts", values=values, labels=labels, legend=legend)
	